# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-01-22 20:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AllergiesAdverseReactionAlert',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('substance', models.CharField(max_length=250, verbose_name='Substancia')),
                ('substance_code', models.CharField(blank=True, max_length=250, null=True, verbose_name='')),
                ('reaction', models.CharField(max_length=250, verbose_name='Reacción')),
                ('status', models.CharField(choices=[('A', 'Activo'), ('I', 'Inactivo')], default='A', max_length=250, verbose_name='Estatus')),
                ('private', models.BooleanField(default=True, verbose_name='Estado')),
            ],
            options={
                'verbose_name_plural': 'Allergies, Adverse Reaction, Alerts',
                'verbose_name': 'Allergies, Adverse Reaction, Alert',
            },
        ),
        migrations.CreateModel(
            name='Reaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Reactions',
                'verbose_name': 'Reaction',
            },
        ),
        migrations.CreateModel(
            name='Substance',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250, unique=True)),
                ('code', models.CharField(max_length=250, unique=True)),
            ],
            options={
                'verbose_name_plural': 'Substances',
                'verbose_name': 'Substance',
            },
        ),
    ]
