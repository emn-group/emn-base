from django.conf.urls import url

from .views import PatientEmergencyDetailView, PatientSearchFormView

urlpatterns = [
    url(r'^perfil/(?P<username>\w{0,50})/(?P<key>\w{0,50})$',
        PatientEmergencyDetailView.as_view(), name='emergency-profile'),
    url(r'^buscar/$',
        PatientSearchFormView.as_view(), name='emergency-search'),
]
