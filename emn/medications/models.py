# -*- coding: utf-8 -*-
from django.db import models

from patients.models import Patient

# Create your models here.
# <table border="1" width="100%">
#       <thead>
#           <tr><th>Medication</th><th>Instructions</th><th>Start Date</th><th>Status</th></tr>
#       </thead>
#       <tbody>
#           <tr><td>Albuterol inhalant</td><td>2 puffs QID PRN wheezing</td><td>&#160;</td><td>Active</td></tr>
#           <tr><td>Clopidogrel (Plavix)</td><td>75mg PO daily</td><td>&#160;</td><td>Active</td></tr>
#           <tr><td>Metoprolol</td><td>25mg PO BID</td><td>&#160;</td><td>Active</td></tr>
#           <tr><td>Prednisone</td><td>20mg PO daily</td><td>Mar 28, 2000</td><td>Active</td></tr>
#           <tr><td>Cephalexin (Keflex)</td><td>500mg PO QID x 7 days (for bronchitis)</td><td>Mar 28, 2000</td><td>No longer active</td></tr>
#       </tbody>
#   </table>


class MedicationName(models.Model):

    class Meta:
        verbose_name = "Medicamento"
        verbose_name_plural = "Medicamentos"

    name = models.CharField(max_length=250, unique=True, verbose_name="Nombre")

    def __str__(self):
        return self.name


ACTIVE_FEED = 'A'
INACTIVE_FEED = 'I'

STATUS_CHOICES = (
    (ACTIVE_FEED, 'Activo'),
    (INACTIVE_FEED, 'Inactivo'),
)


class Medication(models.Model):

    class Meta:
        verbose_name = "Medication"
        verbose_name_plural = "Medications"

    patient = models.ForeignKey(Patient, null=True)
    private = models.BooleanField(default=True, verbose_name = "Compartido")
    start_date = models.DateField(blank=True, null=True, verbose_name = "Fecha de inicio")
    status = models.CharField(max_length=250,
                              choices=STATUS_CHOICES,
                              default=ACTIVE_FEED,
                              verbose_name = "Estatus")
    instructions = models.CharField(max_length=250,
                                    verbose_name = "instrucciones",
                                    blank=True, null=True)
    medication = models.CharField(max_length=250, verbose_name = "Medicamento")

    def __str__(self):
        return self.medication
