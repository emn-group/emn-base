import re
from datetime import datetime, timezone

from django.http import HttpResponseRedirect
from django.utils.deprecation import MiddlewareMixin

from emn.settings import PASSWORD_EXPIRE

from .models import AdminUserProfile


class PasswordChangeMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if request.user.is_authenticated() and \
                re.match(r'^/admin/?', request.path) and \
                not re.match(r'^/admin/password_change/?', request.path):

            profile, created = AdminUserProfile.objects.get_or_create(user=request.user)
            current_datetime = datetime.now(timezone.utc)
            last = (current_datetime - profile.password_date).seconds
            if last > PASSWORD_EXPIRE and not request.user.is_superuser:
                return HttpResponseRedirect('/admin/password_change/')
