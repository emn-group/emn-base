from django.contrib import admin
from .models import Substance,  Reaction
# Register your models here.

class SubstanceModelAdmin(admin.ModelAdmin):
    """
    Settings for registering Substaces
    """
    prepopulated_fields = {"name": ("name",)}
    list_display = ('name',)
    search_fields = ('name', 'code')
    ordering = ('name',)
    # date_hierarchy = 'created_at'


class ReactionModelAdmin(admin.ModelAdmin):
    """
    Settings for registering reactions to Substances
    """
    prepopulated_fields = {"name": ("name",)}
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)
    # date_hierarchy = 'created_at'

admin.site.register(Substance, SubstanceModelAdmin)
admin.site.register(Reaction, ReactionModelAdmin)

