from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import UpdateView, DetailView, DetailView
from django.core.urlresolvers import reverse_lazy
from django.utils.crypto import get_random_string
from contrib.views import AlertMessagesMixin
# from wkhtmltopdf.views import PDFTemplateView
from .forms import PatientDemographicEditForm, PatientEmergencyContact

from .models import Patient

from contrib.views import CheckPatientMixin

from medications.models import Medication
from alerts.models import AllergiesAdverseReactionAlert
from familyhistory.models import FamilyHistory
from procedures.models import Procedure
from problems.models import Problem


# from weasyprint import HTML, CSS
# Create your views here.

class PatientUpdateView(CheckPatientMixin, AlertMessagesMixin, UpdateView):
    model_class = Patient
    template_name = 'patients/edit.html'
    form_class = PatientDemographicEditForm
    success_url = reverse_lazy('patients:profile-patient')

    def get_object(self, queryset=None):
        user = self.request.user
        model = self.model_class
        obj = model.objects.get(user=user)
        return obj


class PatientDetailView(CheckPatientMixin, AlertMessagesMixin, DetailView):
    model_class = Patient
    template_name = 'patients/profile.html'
    context_object_name = 'patient'

    def get_object(self, queryset=None):
        user = self.request.user
        model = self.model_class
        obj = model.objects.get(user=user)
        return obj

    def get_context_data(self, **kwargs):
        ctx = super(PatientDetailView, self).get_context_data(**kwargs)
        patient = self.get_object()
        try:
            problems = Problem.objects.filter(patient=patient)[:5]
            ctx['problems'] = problems
        except:
            pass
        try:
            medication = Medication.objects.filter(patient=patient)[:5]
            ctx['medications'] = medication
        except:
            pass
        try:
            alerts = AllergiesAdverseReactionAlert.objects.filter(
                patient=patient)[:5]
            ctx['alerts'] = alerts
        except:
            pass
        try:
            procedures = Procedure.objects.filter(patient=patient)[:5]
            ctx['procedures'] = procedures
        except:
            pass
        try:
            familyhistory = FamilyHistory.objects.filter(patient=patient)[:5]
            ctx['familyhistory'] = familyhistory
        except:
            pass
        return ctx


class PatientEmergencyPdfView(CheckPatientMixin, DetailView):

    model = Patient
    template_name = 'patients/pdf-emergency.html'
    filename = 'my_pdf.pdf'

    def get_object(self, queryset=None):
        model = self.model
        user = self.request.user
        obj = model.objects.get(user=user)
        obj.card_created = False
        obj.save()
        return obj

    def get_context_data(self, **kwargs):
        ctx = super(PatientEmergencyPdfView,
                    self).get_context_data(**kwargs)
        patient = self.get_object()
        ctx['patient'] = patient
        try:
            problems = Problem.objects.filter(patient=patient, private=False)
            ctx['problems'] = problems
        except:
            pass
        try:
            medication = Medication.objects.filter(patient=patient, private=False)
            ctx['medications'] = medication
        except:
            pass
        try:
            alerts = AllergiesAdverseReactionAlert.objects.filter(
                patient=patient, private=False
            )
            ctx['alerts'] = alerts
        except:
            pass
        try:
            procedures = Procedure.objects.filter(patient=patient, private=False)
            ctx['procedures'] = procedures
        except:
            pass
        try:
            familyhistory = FamilyHistory.objects.filter(patient=patient, private=False)
            ctx['family_history'] = familyhistory
        except:
            pass
        return ctx

    # def render_to_response(self, context, **response_kwargs):
    #     html = super(PatientEmergencyPdfView, self).render_to_response(context, **response_kwargs)
    #     pdf_file = HTML(string=html).write_pdf()
    #     http_response = HttpResponse(pdf_file, content_type='application/pdf')
    #     http_response['Content-Disposition'] = 'filename="report.pdf"'

    #     return http_response


class PatientEmergencyMiniPdfView(CheckPatientMixin, DetailView):

    model = Patient
    template_name = 'patients/pdf-emergency-mini.html'
    filename = 'my_pdf.pdf'

    def get_object(self, queryset=None):
        model = self.model
        user = self.request.user
        obj = model.objects.get(user=user)
        obj.card_created = False
        obj.save()
        return obj

    def get_context_data(self, **kwargs):
        ctx = super(PatientEmergencyMiniPdfView,
                    self).get_context_data(**kwargs)
        patient = self.get_object()
        ctx['patient'] = patient
        return ctx

    # def render_to_response(self, context, **response_kwargs):
    #     html = super(PatientEmergencyMiniPdfView, self).render_to_response(context, **response_kwargs)
    #     pdf_file = HTML(string=html).write_pdf()
    #     http_response = HttpResponse(pdf_file, content_type='application/pdf')
    #     http_response['Content-Disposition'] = 'filename="report.pdf"'

    #     return http_response


class PatientEmergencyDetailView(CheckPatientMixin, AlertMessagesMixin, DetailView):

    model = Patient
    template_name = 'patients/emergency.html'
    context_object_name = 'patient'

    def get_object(self, queryset=None):
        model = self.model
        user = self.request.user
        obj = model.objects.get(user=user)
        return obj

    def get_context_data(self, **kwargs):
        ctx = super(PatientEmergencyDetailView,
                    self).get_context_data(**kwargs)
        patient = self.get_object()
        ctx['patient'] = patient
        try:
            problems = Problem.objects.filter(patient=patient, private=False)
            ctx['problems'] = problems
        except:
            pass
        try:
            medication = Medication.objects.filter(patient=patient, private=False)
            ctx['medications'] = medication
        except:
            pass
        try:
            alerts = AllergiesAdverseReactionAlert.objects.filter(
                patient=patient, private=False
            )
            ctx['alerts'] = alerts
        except:
            pass
        try:
            procedures = Procedure.objects.filter(patient=patient, private=False)
            ctx['procedures'] = procedures
        except:
            pass
        try:
            familyhistory = FamilyHistory.objects.filter(patient=patient, private=False)
            ctx['family_history'] = familyhistory
        except:
            pass
        return ctx


class ContactUpdateView(CheckPatientMixin, AlertMessagesMixin, UpdateView):

    model_class = Patient
    template_name = 'patients/contact.html'
    form_class = PatientEmergencyContact
    success_url = reverse_lazy('patients:profile-patient')

    def get_object(self, queryset=None):
        user = self.request.user
        model = self.model_class
        obj = model.objects.get(user=user)
        return obj


class EmergencyCodeView(CheckPatientMixin, AlertMessagesMixin, DetailView):
    template_name = 'patients/update_emergency_code.html'
    model = Patient
    context_object_name = 'patient'


    def post(self, request, *args, **kwargs):

        patient = Patient.objects.get(user=request.user)
        patient.emergency_code = get_random_string(length=12, allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
        patient.card_created = True
        patient.save()
        return self.get(self, request, *args, **kwargs)

    def get_object(self, queryset=None):

        model = self.model
        user = self.request.user
        obj = model.objects.get(user=user)
        return obj
