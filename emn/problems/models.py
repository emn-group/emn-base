# -*- coding: utf-8 -*-
from django.db import models
from patients.models import Patient
from datetime import datetime
# Create your models here.

# <table border="1" width="100%">
#   <thead>
#       <tr><th>Condition</th><th>Effective Dates</th><th>Condition Status</th></tr>
#   </thead>
#   <tbody>
#       <tr><td>Asthma</td><td>1950</td><td>Active</td></tr>
#       <tr><td>Pneumonia</td><td>Jan 1997</td><td>Resolved</td></tr>
#       <tr><td>"</td><td>Mar 1999</td><td>Resolved</td></tr>"
#       <tr><td>Myocardial Infarction</td><td>Jan 1997</td><td>Resolved</td></tr>           
#   </tbody>
# </table>


class Condition(models.Model):

    class Meta:
        verbose_name = "Condición"
        verbose_name_plural = "Condiciones"
        unique_together = (("name", "code"),)

    name = models.CharField(max_length=250, verbose_name="Nombre")
    code = models.CharField(max_length=250, verbose_name="Código")

    def __str__(self):
        return self.name


ACTIVE_FEED = 'A'
INTERMITTENT_FEED = 'I'
RESOLVED_FEED = 'R'

STATUS_CHOICES = (
    (ACTIVE_FEED, 'Actual: Actualmente tiene esto'),
    (INTERMITTENT_FEED, 'Intermitente: Va y viene'),
    (RESOLVED_FEED, 'Pasado: Ya no tiene esto'),
)

YEAR = tuple((str(n), str(n)) for n in reversed(range(1897, datetime.now().year + 1)))

class Problem(models.Model):

    class Meta:
        verbose_name = "Problem"
        verbose_name_plural = "Problems"
    private = models.BooleanField(default=True, verbose_name="Estado")
    patient = models.ForeignKey(Patient, null=True)
    condition_code = models.CharField(max_length=250, blank=True, verbose_name="Código")
    condition = models.CharField(max_length=250, verbose_name="Condición")
    year_of_onset = models.CharField(
        max_length=4, blank=True, null=True, verbose_name="Año de aparición",
        choices=YEAR)
    year_of_resolution = models.CharField(
        max_length=4, blank=True, null=True, verbose_name="Año de resolución",
        choices=YEAR)
    condition_status = models.CharField(max_length=250,
                                        choices=STATUS_CHOICES,
                                        default=ACTIVE_FEED,
                                        verbose_name = "Estatus")

    def __str__(self):
        return self.condition
