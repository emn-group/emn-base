from django.conf.urls import url
from django.contrib.auth import views as auth_views


from . import views

urlpatterns = [
    # create User
    url(r'^login/$', views.user_login_view,
        name='auth_login'),
    # authentication views
    url(r'^crear/$', views.PatientCreate.as_view(),
        name='auth_patient_create'),
    url(r'^editar/$', views.PatientUpdateView.as_view(),
        name='auth_patient_edit'),
    url(r'^redirect/$', views.user_redirect,
        name='auth_redirect'),
    url(r'^logout/$', auth_views.logout_then_login,
        name='auth_logout'),
    url(r'^password-change/$',
        auth_views.password_change,
        name='change_password'),
    url(r'^password-change/done/$',
        auth_views.password_change_done),
    url(r'^password-reset/$',
        auth_views.password_reset, {
            'template_name': 'users/password-reset.html',
            'email_template_name':'users/password-reset-email.html',
            'post_reset_redirect':'users:reset-password-done',
            },
        'password-reset'),
    url(r'^password-reset/done/$',
        auth_views.password_reset_done, {
            'template_name': 'users/password-reset-done.html'},
        name='reset-password-done',
       ),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$',
        auth_views.password_reset_confirm, {
            'template_name': 'users/password-reset-confirm.html',
            'post_reset_redirect':'users:auth_login',
            },
        name='password-reset-confirm'),
    url(r'^reset/done/$',
        auth_views.password_reset_complete),
]
