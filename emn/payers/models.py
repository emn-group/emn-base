# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# REFERENCE
# <tr><th>Payer name</th><th>Policy type / Coverage type</th><th>Covered party ID</th> <th>Authorization(s)</th></tr>
# <tr>
#   <td>Good Health Insurance</td> 
#   <td>Extended healthcare / Self</td> 
#   <td>14d4a520-7aae-11db-9fe1-0800200c9a66</td>
#   <td>Colonoscopy</td>
# </tr>
