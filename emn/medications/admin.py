from django.contrib import admin
from .models import MedicationName
# Register your models here.


class MedicationNameModelAdmin(admin.ModelAdmin):
    """
    Settings for registering family related diagnoses
    """
    prepopulated_fields = {"name": ("name",)}
    list_display = ('name',)
    search_fields = ('name',)
    ordering = ('name',)
    # date_hierarchy = 'created_at'

admin.site.register(MedicationName, MedicationNameModelAdmin)