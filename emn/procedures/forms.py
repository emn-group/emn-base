# -*- coding: utf-8 -*-
from django.forms import ModelForm, RadioSelect, TypedChoiceField
from django.forms.widgets import HiddenInput

from .models import Procedure


class ProcedureCreateUpdateForm(ModelForm):
    """
    Form that creates and edits patient`s Procedures
    """
    YESNO_CHOICES = (('0', 'Compartido'), ('1', 'Privado'))
    private = TypedChoiceField(choices=YESNO_CHOICES, widget=RadioSelect, label='Estado',
                               help_text='La información marcada como privada no puede ser accedida por el personal médico')

    class Meta:

        model = Procedure
        fields = ('procedure',
                  'procedure_code',
                  'date',
                  'private',)
        widgets = {'procedure_code': HiddenInput}


