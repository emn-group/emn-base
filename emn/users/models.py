# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import AbstractUser, PermissionsMixin,\
    UserManager, Group

from datetime import datetime

# Object level Permissions

from guardian.mixins import GuardianUserMixin

# Transactions

from django.db import transaction

# settings

from emn.settings import AUTH_USER_MODEL


#  Models


class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser, PermissionsMixin, GuardianUserMixin):
    email = models.EmailField(
        verbose_name='Correo',
        max_length=255,
    )
    date_of_birth = models.DateField(null=True, verbose_name='Fecha de Nacimiento')
    is_active = models.BooleanField(default=True, verbose_name='Activo')
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    identity_document = models.CharField(
        max_length=100, unique=True, null=True, verbose_name='Documento de Identidad')

    USERNAME_FIELD = 'username'

    objects = CustomUserManager()
    REQUIRED_FIELDS = ['email']


class AdminUserProfile(models.Model):

    password_date = models.DateTimeField(
        blank=True, default=datetime.now())
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE,
                             related_name='user_profile')


class PatientProxy(CustomUser):

    class Meta:
        verbose_name = "Paciente"
        verbose_name_plural = "Pacientes"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        super(PatientProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = PatientProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='patient')
            user.groups.add(group)


class MedicalPersonnelProxy(CustomUser):

    class Meta:
        verbose_name = "Personal médico"
        verbose_name_plural = "Personal médico"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        super(MedicalPersonnelProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = MedicalPersonnelProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='medical-personnel')
            user.groups.add(group)


class AdministrativePersonnelProxy(CustomUser):

    class Meta:
        verbose_name = "Personal administrativo"
        verbose_name_plural = "Personal administrativo"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        super(AdministrativePersonnelProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = AdministrativePersonnelProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='administrative-personnel')
            user.groups.add(group)


class PatientManagerProxy(CustomUser):

    class Meta:
        verbose_name = "Manejador de pacientes"
        verbose_name_plural = "Manejadores de pacientes"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.is_staff:
            self.is_staff = True
        super(PatientManagerProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = PatientManagerProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='patient-manager')
            user.groups.add(group)


class InstitutionProxy(CustomUser):

    class Meta:
        verbose_name = "Institución"
        verbose_name_plural = "Instituciones"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        super(InstitutionProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = InstitutionProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='institution')
            user.groups.add(group)


class PersonnelManagerProxy(CustomUser):

    class Meta:
        verbose_name = "Manejador de personal"
        verbose_name_plural = "Manejadores de personal"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.is_staff:
            self.is_staff = True
        super(PersonnelManagerProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = PersonnelManagerProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='personnel-manager')
            user.groups.add(group)


class InstitutionManager(CustomUser):

    class Meta:
        verbose_name = "Manejador de instituciones"
        verbose_name_plural = "Manejadores de instituciones"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.is_staff:
            self.is_staff = True
        super(InstitutionManager, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = InstitutionManager.objects.get(pk=self.pk)
            user.is_staff = True
            group = Group.objects.get(name='institution-manager')
            user.groups.add(group)


class AdminManagerProxy(CustomUser):

    class Meta:
        verbose_name = "Administrador de manejadores"
        verbose_name_plural = "Administradores de manejadores"
        proxy = True

    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.is_staff:
            self.is_staff = True
        super(AdminManagerProxy, self).save(*args, **kwargs)
        if getattr(self, 'pk', True):
            user = AdminManagerProxy.objects.get(pk=self.pk)
            group = Group.objects.get(name='manager')
            user.groups.add(group)
