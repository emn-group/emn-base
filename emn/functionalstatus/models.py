# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <table border="1" width="100%">
#       <thead>
#       <tr><th>Functional Condition</th> <th>Effective Dates</th> <th>Condition Status</th></tr>
#       </thead>
#       <tbody>
#       <tr><td>Dependence on cane</td><td>1998</td><td>Active</td></tr>
#       <tr><td>Memory impairment</td><td>1999</td><td>Active</td></tr>
#       </tbody>
# </table>

ACTIVE_FEED = 'A'
INACTIVE_FEED = 'I'

STATUS_CHOICES = (
    (ACTIVE_FEED, 'Active'),
    (INACTIVE_FEED, 'Inactive'),
)


class FunctionalCondition(models.Model):

    class Meta:
        verbose_name = "Functional Condition"
        verbose_name_plural = "Functional Condition"

    name = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.names


class FunctionalStatus(models.Model):

    class Meta:
        verbose_name = "Functional Status"
        verbose_name_plural = "Functional Status"

    functional_condition = models.CharField(max_length=250)
    condition_status = models.CharField(max_length=250,
                                        choices=STATUS_CHOICES,
                                        default=ACTIVE_FEED)
    effective_dates = models.CharField(max_length=250, )

    def __str__(self):
        return self.functional_condition
