from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import (CustomUser, MedicalPersonnelProxy, AdministrativePersonnelProxy,
                     AdminManagerProxy, PatientManagerProxy, InstitutionProxy,
                     PersonnelManagerProxy, InstitutionManager, PatientProxy)

from .forms import UserCreationForm, UserChangeForm, UserProxyCreateUpdateForm



class UserAdmin(BaseUserAdmin):

    """The forms to add and change user instances"""
    form = UserChangeForm
    add_form = UserCreationForm


class UserProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'


class MedicProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(MedicProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "medical-personnel"])

class AdministrativeProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(AdministrativeProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "administrative-personnel"])

class PersonnelProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(PersonnelProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "personnel-manager"])

class InstitutionProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(InstitutionProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "institution-manager"])

class InstitutionManagerProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(InstitutionManagerProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "administrative-personnel"])

class PatientManagerProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(PatientManagerProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "patient-manager"])

class PatientProxyAdmin(admin.ModelAdmin):
    """
    Settings for registering medical users in the admin
    """
    prepopulated_fields = {"username": ("username",)}
    list_display = ('username',)
    search_fields = ('username',)
    form = UserProxyCreateUpdateForm
    ordering = ('username',)
    # date_hierarchy = 'created_at'

    def get_queryset(self, request):
        qs = super(PatientProxyAdmin, self).get_queryset(request)
        return qs.filter(groups__name__in=[
            "patient-manager"])

# Now register the new UserAdmin...
admin.site.register(CustomUser, UserAdmin)
admin.site.register(MedicalPersonnelProxy, MedicProxyAdmin)
admin.site.register(AdministrativePersonnelProxy, AdministrativeProxyAdmin)
# admin.site.register(AdminManagerProxy, AdministrativeProxyAdmin)
# admin.site.register(InstitutionProxy, InstitutionProxyAdmin)
# admin.site.register(InstitutionManager, InstitutionManagerProxyAdmin)
admin.site.register(PersonnelManagerProxy, PersonnelProxyAdmin)
# admin.site.register(PatientManagerProxy, PatientManagerProxy)
# admin.site.register(PatientProxy, PatientProxyAdmin)



