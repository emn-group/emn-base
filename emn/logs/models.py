from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.conf import settings

from patients.models import Patient

LOG_CHOICES = (
    ('C', 'Create'),
    ('E', 'Edit'),
    ('D', 'Delete'),
)

class MedicalLog(models.Model):
    """
    Medical information changes log
    """
    class Meta:
        verbose_name = "Modificación"
        verbose_name_plural = "Modificaciones"
    action_type = models.CharField(max_length=1, choices=LOG_CHOICES)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    patient = models.ForeignKey(Patient)
    date = models.DateTimeField(auto_now_add=True, blank=True)

    def __str__(self):              # __unicode__ on Python 2
        return self.action_type

class EmergencyAccessLog(models.Model):
    """
    Medical information access logs
    """
    class Meta:
        verbose_name = "Acceso a información de emergencia"
        verbose_name_plural = "Accesos a información de emergencia"
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL
    )
    username = models.CharField(max_length=500, default='')
    password = models.CharField(max_length=500, default='')
    success = models.BooleanField(default=False)
    patient = models.ForeignKey(Patient, null=True)
    date = models.DateTimeField(auto_now_add=True, blank=True)


class LoginLog(models.Model):
    """
    Medical information access logs
    """
    class Meta:
        verbose_name = "Inicio de sesión"
        verbose_name_plural = "Inicios de sesión"
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL
    )
    date = models.DateTimeField(auto_now_add=True, blank=True)


