from datetime import datetime

from django.core.exceptions import ObjectDoesNotExist

# from django.db import models
# from django.db.models import signals

from .models import (CustomUser, AdminUserProfile)


def roles_perms(sender, **kwargs):
    """
    Get or Create roles post migrate
    """
    from django.contrib.auth.models import Group
    from django.contrib.auth.models import Permission

    def assign_multiple_permissions(perms, obj):
        """
        Assign multiple Permissions to an Group Obj
        """
        for perm in perms:
            instance = Permission.objects.get(codename=perm)
            obj.permissions.add(instance)

    # External Roles
    patient_role, patient_role_created = Group.objects.get_or_create(
        name="patient")
    medic_role, medic_role_created = Group.objects.get_or_create(
        name="medical-personnel"
    )
    administrative_role, administrative_role_created = Group.objects.get_or_create(
        name="administrative-personnel"
    )
    institution_role, institution_role_created = Group.objects.get_or_create(
        name='institution')

    # Personnel Roles
    personnel_manager_role, personnel_manager_role_created = Group.objects.get_or_create(
        name="personnel-manager"
    )
    institution_manager_role, institution_manager_role_created = Group.objects.get_or_create(
        name='institution-manager'
    )
    manager_role, manager_role_created = Group.objects.get_or_create(
        name='manager'
    )
    patient_manager_role, patient_manager_role_created = Group.objects.get_or_create(
        name='patient-manager'
    )

    personnel_manager_role_perms = (
        'add_medicalpersonnelproxy',
        'change_medicalpersonnelproxy',
        'add_administrativepersonnelproxy',
        'change_administrativepersonnelproxy',
    )

    assign_multiple_permissions(
        perms=personnel_manager_role_perms,
        obj=personnel_manager_role
    )

    institution_manager_role_perms = (
        'add_institutionproxy',
        'change_institutionproxy',
    )
    assign_multiple_permissions(
        perms=institution_manager_role_perms,
        obj=institution_manager_role)

    manager_role_perms = (
        'add_patientmanagerproxy',
        'change_patientmanagerproxy',
        'add_personnelmanagerproxy',
        'change_personnelmanagerproxy',
        'add_institutionmanager',
        'change_institutionmanager',
    )

    assign_multiple_permissions(
        perms=manager_role_perms,
        obj=manager_role
    )

    patient_manager_role_perms = (
        'add_patientproxy',
        'change_patientproxy',
    )

    assign_multiple_permissions(
        perms=patient_manager_role_perms,
        obj=patient_manager_role
    )


def create_user_profile_signal(sender, instance, created, **kwargs):
    if created:
        AdminUserProfile.objects.create(user=instance)


def password_change_signal(sender, instance, **kwargs):

    try:
        user = CustomUser.objects.get(username=instance.username)

        if not user.password == instance.password:
            profile = AdminUserProfile.objects.get_or_create(user=user)
            profile.password_date = datetime.now()
            profile.save()
    except AdminUserProfile.DoesNotExist:
        pass





