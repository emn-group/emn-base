# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-03-02 16:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('procedures', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='procedurenamecode',
            options={'verbose_name': 'Procedimiento', 'verbose_name_plural': 'Procedimientos'},
        ),
        migrations.AlterField(
            model_name='procedurenamecode',
            name='code',
            field=models.CharField(max_length=250, unique=True, verbose_name='Código'),
        ),
        migrations.AlterField(
            model_name='procedurenamecode',
            name='name',
            field=models.CharField(max_length=250, unique=True, verbose_name='Nombre'),
        ),
    ]
