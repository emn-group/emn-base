# -*- coding: utf-8 -*-
from django.db import models

from patients.models import Patient

# Create your models here.
# <thead>
#   <tr><th>Substance</th><th>Reaction</th><th>Status</th></tr>
# </thead>
# <tbody>
#   <tr><td>Penicillin</td><td>Hives</td><td>Active</td></tr>
#   <tr><td>Aspirin</td><td>Wheezing</td><td>Active</td></tr>
#   <tr><td>Codeine</td><td>Nausea</td><td>Active</td></tr>
# </tbody>


class Substance(models.Model):

    class Meta:
        verbose_name = "Sustancia"
        verbose_name_plural = "Sustancias"

    name = models.CharField(max_length=250, unique=True, verbose_name="Nombre")
    code = models.CharField(max_length=250, unique=True, verbose_name="Código")

    def __str__(self):
        return self.name


class Reaction(models.Model):

    class Meta:
        verbose_name = "Reacción"
        verbose_name_plural = "Reacciones"

    name = models.CharField(max_length=250, unique=True, verbose_name="Nombre")

    def __str__(self):
        return self.name


ACTIVE_FEED = 'A'
INACTIVE_FEED = 'I'

STATUS_CHOICES = (
    (ACTIVE_FEED, 'Activo'),
    (INACTIVE_FEED, 'Inactivo'),
)


class AllergiesAdverseReactionAlert(models.Model):

    class Meta:
        verbose_name = "Allergies, Adverse Reaction, Alert"
        verbose_name_plural = "Allergies, Adverse Reaction, Alerts"

    patient = models.ForeignKey(Patient, null=True)
    substance = models.CharField(max_length=250, verbose_name="Substancia")
    substance_code = models.CharField(max_length=250, verbose_name="", blank=True, null=True)
    reaction = models.CharField(max_length=250, verbose_name="Reacción", blank=True, null=True)
    status = models.CharField(max_length=250,
                              choices=STATUS_CHOICES,
                              default=ACTIVE_FEED,
                              verbose_name="Estatus")
    private = models.BooleanField(
        default=True,
        verbose_name="Estado",
        )

    def __str__(self):
        return self.substance
