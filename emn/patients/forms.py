# -*- coding: utf-8 -*-
from datetime import datetime
from .models import Patient

from django.forms import BaseInlineFormSet, inlineformset_factory, ModelForm, ValidationError

from users.forms import PatientCreationForm

from users.models import PatientProxy



class PatientDemographicEditForm(ModelForm):
    """
    Form that edits Patient`s demographic information
    """
    class Meta:
        model = Patient
        fields = ('first_name', 'last_name', 'identity_document', 'date_of_birth', 'gender',
                  'blood_type', 'rhesus_factor', 'phone_number', 'nationality', 'occupation',
                  'home_address', 'birth_certificate_number', 'birth_certificate_folio_number',
                  'birth_certificate_volume_number', 'birth_certificate_book_number')


    def clean_rhesus_factor(self):

        blood_type = self.cleaned_data.get("blood_type")
        rhesus_factor = self.cleaned_data.get("rhesus_factor")
        if rhesus_factor:
            if not blood_type:
                raise ValidationError(
                    'No se puede introducir un factor Rh sin un grupo sanguíneo')
        return self.cleaned_data.get("rhesus_factor")

    def clean_date_of_birth(self):
        date_of_birth = self.cleaned_data.get("date_of_birth")
        if date_of_birth:
            if date_of_birth > datetime.date(datetime.now()):
                raise ValidationError(
                    'No se puede introducir una fecha de nacimiento en el futuro')
        return self.cleaned_data.get("date_of_birth")

    def clean(self):
        # Check that 'birth_certificate_number',
        # 'birth_certificate_folio_number', 'birth_certificate_volume_number',
        # 'birth_certificate_book_number' exist together
        super(PatientDemographicEditForm, self).clean()
        birth_certificate_number = self.cleaned_data.get(
            "birth_certificate_number")
        birth_certificate_folio_number = self.cleaned_data.get(
            "birth_certificate_folio_number")
        birth_certificate_volume_number = self.cleaned_data.get(
            "birth_certificate_volume_number")
        birth_certificate_book_number = self.cleaned_data.get(
            "birth_certificate_book_number")

        if (birth_certificate_number or birth_certificate_folio_number
                or birth_certificate_volume_number or birth_certificate_book_number):
            if not (birth_certificate_number and birth_certificate_folio_number
                    and birth_certificate_volume_number and birth_certificate_book_number):
                e = ValidationError(
                    "Los Números del Acta de Nacimiento, de Tomo, de Folio, de Libro deben ser llenados")
                self.add_error(None, e)
                if any(self.errors):
                    print(self.errors)





class PatientEmergencyContact(ModelForm):
    """
    Form that edits Patient`s emergency contact information
    """
    class Meta:
        model = Patient
        fields = ('contact_first_name', 'contact_last_name', 'contact_phone_number')
