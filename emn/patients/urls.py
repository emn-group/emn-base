from django.conf.urls import url

from .views import (PatientUpdateView, PatientDetailView, PatientEmergencyDetailView,
                    PatientEmergencyPdfView, ContactUpdateView, EmergencyCodeView,
                    PatientEmergencyMiniPdfView)

urlpatterns = [
    url(r'^emergencia/codigo/$', EmergencyCodeView.as_view(), name='emergency-code'),
    url(r'^editar/$', PatientUpdateView.as_view(), name='edit-patient'),
    url(r'^contacto/$', ContactUpdateView.as_view(), name='contact'),
    url(r'^perfil/$', PatientDetailView.as_view(), name='profile-patient'),
    url(r'^emergencia/$', PatientEmergencyDetailView.as_view(), name='emergency-patient'),
    url(r'^pdf/$', PatientEmergencyPdfView.as_view(), name='emergency-pdf'),
    url(r'^pdf/mini/$', PatientEmergencyMiniPdfView.as_view(), name='emergency-pdf-mini'),

]
