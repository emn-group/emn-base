# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext as _
from django.utils.crypto import get_random_string


from emn.settings import AUTH_USER_MODEL


# Create your models here.


A = 'A'
B = 'B'
AB = 'AB'
O = 'O'

BLOOD_TYPE = (
    (A, 'A'),
    (B, 'B'),
    (AB, 'AB'),
    (O, 'O'),
)

NEGATIVE = '-'
POSITIVE = '+'

RHESUS_FACTOR = (
    (NEGATIVE, '-'),
    (POSITIVE, '+'),
)

class Patient(models.Model):
    user = models.OneToOneField(AUTH_USER_MODEL, on_delete=models.CASCADE,
                                related_name='user')
    emergency_code = models.CharField(max_length=50,
                                      verbose_name='Código',
                                      default=get_random_string(
                                          length=12,
                                          allowed_chars='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'))
    first_name = models.CharField(max_length=500,
                                  verbose_name='Nombre',
                                  blank=False,
                                  null=False)
    last_name = models.CharField(max_length=500,
                                 verbose_name='Apellido',
                                 blank=False,
                                 null=False)
    date_of_birth = models.DateField(null=False,
                                     verbose_name='Fecha de nacimiento',
                                     blank=False,)
    blood_type = models.CharField(
        max_length=2,
        choices=BLOOD_TYPE,
        verbose_name='Grupo sanguíneo', null=True, blank=True
    )
    rhesus_factor = models.CharField(
        max_length=2,
        choices=RHESUS_FACTOR,
        verbose_name='Factor rh', null=True, blank=True
    )
    identity_document = models.CharField(max_length=100,
                                         verbose_name='Documento de Identificación',
                                         null=True,
                                         blank=True)
    phone_number = models.CharField(max_length=50,
                                    verbose_name='Número de teléfono',
                                    null=True, blank=True)
    gender = models.CharField(
        max_length=50,
        choices=(
            ('F', _('Femenino')),
            ('M', _('Masculino')),
        ),
        verbose_name='Sexo', null=True, blank=False
    )
    nationality = models.CharField(
        max_length=50,
        choices=(
            ('V', _('Venezolano')),
            ('E', _('Extranjero'))
        ),
        verbose_name='Nacionalidad', null=True, blank=True
    )
    occupation = models.TextField(verbose_name='Ocupación', null=True,
                                  blank=True)
    home_address = models.TextField(verbose_name='Dirección de Habitación',
                                    null=True, blank=True)
    contact_first_name = models.CharField(max_length=500,
                                          verbose_name='Nombre',
                                          blank=False,
                                          null=True)
    contact_last_name = models.CharField(max_length=500,
                                         verbose_name='Apellido',
                                         blank=False,
                                         null=True)
    contact_phone_number = models.CharField(max_length=50,
                                            verbose_name='Número de teléfono',
                                            null=True, blank=False)
    card_created = models.BooleanField(
        default=True,
        verbose_name="card created"
        )
    birth_certificate_number = models.PositiveIntegerField(
        verbose_name='Número del Acta de Nacimiento',
        null=True, blank=True)
    birth_certificate_folio_number = models.PositiveIntegerField(verbose_name='Número de Tomo',
                                                                 null=True, blank=True)
    birth_certificate_volume_number = models.PositiveIntegerField(verbose_name='Número de Folio',
                                                                  null=True, blank=True)
    birth_certificate_book_number = models.PositiveIntegerField(verbose_name='Número de Libro',
                                                                null=True, blank=True)
    # father_first_name = models.CharField(max_length=500,
    #                                      verbose_name='Nombre del padre',
    #                                      blank=False,
    #                                      null=False)
    # father_last_name = models.CharField(max_length=500,
    #                                     verbose_name='Apellido del padre',
    #                                     blank=False,
    #                                     null=False)
    # father_identity_document = models.CharField(
    #     max_length=100,
    #     verbose_name='Documento de Identificación del padre',
    #     null=True,
    #     blank=True)
    # mother_first_name = models.CharField(max_length=500,
    #                                      verbose_name='Nombre de la madre',
    #                                      blank=False,
    #                                      null=False)
    # mother_last_name = models.CharField(max_length=500,
    #                                     verbose_name='Apellido de la madre',
    #                                     blank=False,
    #                                     null=False)
    # mother_identity_document = models.CharField(
    #     max_length=100,
    #     verbose_name='Documento de Identificación de la madre',
    #     null=True,
    #     blank=True)


