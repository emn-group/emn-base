from django.contrib import admin
from .models import ProcedureNameCode
# Register your models here.

class ProcedureNameCodeModelAdmin(admin.ModelAdmin):
    """
    Settings for registering family related diagnoses
    """
    prepopulated_fields = {"name": ("name",)}
    list_display = ('name',)
    search_fields = ('name', 'code')
    ordering = ('name',)
    # date_hierarchy = 'created_at'

admin.site.register(ProcedureNameCode, ProcedureNameCodeModelAdmin)
