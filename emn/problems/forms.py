# -*- coding: utf-8 -*-
from django.forms import ModelForm, RadioSelect, TypedChoiceField, ValidationError
from django.forms.widgets import HiddenInput, RadioSelect

from .models import Problem


class ProblemCreateUpdateForm(ModelForm):
    """
    Form that creates and edits patient`s problems
    """
    YESNO_CHOICES = (('0', 'Compartido'), ('1', 'Privado'))
    private = TypedChoiceField(choices=YESNO_CHOICES, widget=RadioSelect, label='Estado',
                               help_text='La información marcada como privada no puede ser accedida por el personal médico')

    class Meta:

        model = Problem
        fields = ('condition',
                  'condition_code',
                  'year_of_onset',
                  'year_of_resolution',
                  'condition_status',
                  'private')
        widgets = {'condition_code': HiddenInput}

    def clean(self):
        # Check that year_of_onset is lower than year_of_resolution
        super(ProblemCreateUpdateForm, self).clean()
        try:
            year_of_resolution = int(
                self.cleaned_data.get("year_of_resolution"))
            year_of_onset = int(self.cleaned_data.get("year_of_onset"))
        except TypeError:
            year_of_resolution = None
            year_of_onset = None
        if year_of_onset and year_of_resolution:
            if year_of_resolution < year_of_onset:
                raise ValidationError(
                    "El año de aparición no puede ser mayor que el año de resolución")

        year_of_resolution = self.cleaned_data.get("year_of_resolution")
        condition_status = self.cleaned_data.get("condition_status")
        if year_of_resolution and condition_status == 'A':
            raise ValidationError(
                "De tener año de resolución la condición no debe colocarse en estatus activo")


