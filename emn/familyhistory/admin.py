from django.contrib import admin
from .models import Diagnosis
# Register your models here.

class DiagnosisModelAdmin(admin.ModelAdmin):
    """
    Settings for registering family related diagnoses
    """
    prepopulated_fields = {"name": ("name",)}
    list_display = ('name',)
    search_fields = ('name', 'code')
    ordering = ('name',)
    # date_hierarchy = 'created_at'

admin.site.register(Diagnosis, DiagnosisModelAdmin)