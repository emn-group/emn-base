
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from django.core.urlresolvers import reverse_lazy

from django.db import transaction

from django.contrib.messages.views import SuccessMessageMixin

from patients.models import Patient

from contrib.views import AddPatientCreateMixin, ListPatientObjectsMixin, AlertMessagesMixin, CheckPatientMixin

from logs.views import RegisterActionMixing

from .models import Relative, FamilyHistory

from .forms import RelativeCreateUpdateForm, FamilyHistoryCreateUpdateForm
# Create your views here.


class RelativeCreateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, AddPatientCreateMixin, CreateView):
    model_class = Relative
    template_name = 'relatives/create-edit.html'
    form_class = RelativeCreateUpdateForm
    success_url = reverse_lazy('familyhistory:list-relative')
    success_message = "%(first_name)s fue creado satisfactoriamente"


class RelativeListView(CheckPatientMixin, ListPatientObjectsMixin, AlertMessagesMixin, ListView):
    model = Relative
    template_name = 'relatives/list.html'
    context_object_name = 'relatives'

    def get_context_data(self, **kwargs):
        user = self.request.user
        ctx = super(RelativeListView, self).get_context_data(**kwargs)
        patient = Patient.objects.filter(user=user)
        try:
            familyhistory = FamilyHistory.objects.filter(patient=patient)
            ctx['family_history'] = familyhistory
        except:
            pass
        return ctx


class RelativeUpdateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, UpdateView):
    model = Relative
    template_name = 'relatives/create-edit.html'
    form_class = RelativeCreateUpdateForm
    success_url = reverse_lazy('familyhistory:list-relative')
    success_message = "%(first_name)s fue editado satisfactoriamente"



class RelativeDeleteView(DeleteView):
    model = Relative
    form_class = RelativeCreateUpdateForm
    success_url = reverse_lazy('familyhistory:list-relative')
    

class FamilyHistoryCreateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, AddPatientCreateMixin, CreateView):
    model_class = FamilyHistory
    template_name = 'familyhistory/create-edit.html'
    form_class = FamilyHistoryCreateUpdateForm
    success_url = reverse_lazy('familyhistory:list-relative')
    success_message = "%(diagnosis)s fue creado satisfactoriamente"
    def get_form_kwargs(self):
        kwargs = super(FamilyHistoryCreateView, self).get_form_kwargs()
        patient = Patient.objects.get(user=self.request.user)
        kwargs.update({'patient': patient})
        return kwargs


class FamilyHistoryUpdateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, UpdateView):
    model = FamilyHistory
    template_name = 'familyhistory/create-edit.html'
    form_class = FamilyHistoryCreateUpdateForm
    success_url = reverse_lazy('familyhistory:list-relative')
    success_message = "%(diagnosis)s fue editado satisfactoriamente"
    def get_form_kwargs(self):
        kwargs = super(FamilyHistoryUpdateView, self).get_form_kwargs()
        kwargs.update({'patient': self.object.patient})
        return kwargs


class FamilyHistoryDeleteView(DeleteView):
    model = FamilyHistory
    form_class = FamilyHistoryCreateUpdateForm
    success_url = reverse_lazy('familyhistory:list-relative')
