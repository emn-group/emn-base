from django.conf.urls import url

from .views import LandingPageView


urlpatterns = [
    url(r'^home/$', LandingPageView.as_view(),
        name='landing'),
    url(r'^$', LandingPageView.as_view(),
        name='landing-2'),
]