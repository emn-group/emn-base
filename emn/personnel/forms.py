from django.forms import Form, CharField


class PatientSearchForm(Form):
    """
    Base for the Patient`s inline formset
    """
    username = CharField(label='Usuario', max_length=100)
    key = CharField(label='Clave', max_length=100)

