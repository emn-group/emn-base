from django.shortcuts import reverse

from django.views.generic import DetailView, FormView
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from django.contrib.auth.decorators import user_passes_test
from django.utils.decorators import method_decorator

from users.models import CustomUser
from patients.models import Patient

from medications.models import Medication
from alerts.models import AllergiesAdverseReactionAlert
from familyhistory.models import FamilyHistory
from procedures.models import Procedure
from problems.models import Problem
from contrib.views import CheckUserGroupsMixin
from logs.models import EmergencyAccessLog

from .forms import PatientSearchForm



class PatientEmergencyDetailView(CheckUserGroupsMixin, DetailView):

    model = Patient
    template_name = 'personnel/profile.html'
    context_object_name = 'patient'


    def get_object(self, queryset=None):
        username = self.kwargs.get('username')
        key = self.kwargs.get('key')
        model = self.model
        try:
            user = CustomUser.objects.get(username=username)
        except CustomUser.DoesNotExist:
            raise Http404("""No existe un paciente con el nombre de usuario y clave
                            registrados""")
        try:
            obj = model.objects.get(user=user, emergency_code=key)
        except model.DoesNotExist:
            register = EmergencyAccessLog.objects.create(
                user=self.request.user,
                username=username,
                password=key,
                success=False)
            register.save()
            raise Http404("""No existe un paciente con el nombre de usuario y clave
                            registrados""")
        if obj:
            register = EmergencyAccessLog.objects.create(
                user=self.request.user,
                username=username,
                patient=obj,
                password=key,
                success=True)
            register.save()

        return obj

    def get_context_data(self, **kwargs):
        ctx = super(PatientEmergencyDetailView, self).get_context_data(**kwargs)
        patient = self.get_object()
        try:
            problems = Problem.objects.filter(patient=patient, private=False)
            ctx['problems'] = problems
        except:
            pass
        try:
            medication = Medication.objects.filter(
                patient=patient, private=False)
            ctx['medications'] = medication
        except:
            pass
        try:
            alerts = AllergiesAdverseReactionAlert.objects.filter(
                patient=patient, private=False)
            ctx['alerts'] = alerts
        except:
            pass
        try:
            procedures = Procedure.objects.filter(
                patient=patient, private=False)
            ctx['procedures'] = procedures
        except:
            pass
        try:
            familyhistory = FamilyHistory.objects.filter(
                patient=patient, private=False)
            ctx['familyhistory'] = familyhistory
        except:
            pass
        return ctx


class PatientSearchFormView(CheckUserGroupsMixin, FormView):

    form_class = PatientSearchForm
    template_name = 'personnel/search.html'
    context_object_name = 'patient'
    success_url = reverse_lazy('alerts:list-alert')
    form = None

    def form_valid(self, form):
        self.form = form
        return super(PatientSearchFormView, self).form_valid(form)

    def get_success_url(self):
        username = self.form.cleaned_data['username']
        key = self.form.cleaned_data['key']
        return reverse('personnel:emergency-profile', kwargs={'username': username, 'key': key})
