from django.shortcuts import render

from django.contrib.contenttypes.models import ContentType

from .models import MedicalLog, EmergencyAccessLog, LoginLog
# Create your views here.


class RegisterActionMixing(object):
    """A mixing that registers when an user does an action over his medical info"""

    def form_valid(self, form):
        resp = super(RegisterActionMixing, self).form_valid(form)
        obj = self.object
        register = MedicalLog.objects.create(
            content_type=ContentType.objects.get_for_model(obj),
            content_object=obj,
            object_id=obj.pk,
            patient=self.object.patient)
        register.save()
        return resp
