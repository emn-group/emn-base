# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <table border="1" width="100%">
#   <thead>
#       <tr><th>Vaccine</th><th>Date</th><th>Status</th></tr>
#   </thead>
#   <tbody>
#       <tr><td>Influenza virus vaccine, IM</td><td>Nov 1999</td><td>Completed</td></tr>
#       <tr><td>Influenza virus vaccine, IM</td><td>Dec 1998</td><td>Completed</td></tr>
#       <tr><td>Pneumococcal polysaccharide vaccine, IM</td><td>Dec 1998</td><td>Completed</td></tr>
#       <tr><td>Tetanus and diphtheria toxoids, IM</td><td>1997</td><td>Completed</td></tr>
#   </tbody>
# </table>