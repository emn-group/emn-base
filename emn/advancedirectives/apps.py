from django.apps import AppConfig


class AdvancedirectivesConfig(AppConfig):
    name = 'advancedirectives'
