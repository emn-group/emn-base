from django.conf.urls import url
from contrib.views import autocomplete_view
from .models import Diagnosis
from .views import (RelativeCreateView, RelativeListView, RelativeUpdateView, RelativeDeleteView,
                    FamilyHistoryCreateView, FamilyHistoryUpdateView, FamilyHistoryDeleteView)

urlpatterns = [
    url(r'^insertar/$', FamilyHistoryCreateView.as_view(), name='create-familyhistory'),
    url(r'^editar/(?P<pk>[0-9]+)/$', FamilyHistoryUpdateView.as_view(), name='edit-familyhistory'),
    url(r'^borrar/(?P<pk>[0-9]+)/$', FamilyHistoryDeleteView.as_view(), name='delete-familyhistory'),
    url(r'^familiar/insertar/$', RelativeCreateView.as_view(), name='create-relative'),
    url(r'^familiar/listar/$', RelativeListView.as_view(), name='list-relative'),
    url(r'^familiar/editar/(?P<pk>[0-9]+)/$', RelativeUpdateView.as_view(), name='edit-relative'),
    url(r'^familiar/borrar/(?P<pk>[0-9]+)/$', RelativeDeleteView.as_view(), name='delete-relative'),
    url(r'^diagnosis/$', autocomplete_view, {'model': Diagnosis}, name='diagnosis'),

]
