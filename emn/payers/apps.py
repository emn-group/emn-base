from django.apps import AppConfig


class PayersConfig(AppConfig):
    name = 'payers'
