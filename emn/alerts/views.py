
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from django.core.urlresolvers import reverse_lazy

from django.db import transaction

from django.contrib.messages.views import SuccessMessageMixin

from patients.models import Patient

from contrib.views import AddPatientCreateMixin, ListPatientObjectsMixin, AlertMessagesMixin, CheckPatientMixin

from logs.views import RegisterActionMixing

from .models import AllergiesAdverseReactionAlert

from .forms import AlertsCreateUpdateForm
# Create your views here.


class AlertCreateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, AddPatientCreateMixin, CreateView):
    model_class = AllergiesAdverseReactionAlert
    template_name = 'alerts/create-edit.html'
    form_class = AlertsCreateUpdateForm
    success_url = reverse_lazy('alerts:list-alert')
    success_message = "%(substance)s fue creado satisfactoriamente"


class AlertListView(CheckPatientMixin, AlertMessagesMixin, ListPatientObjectsMixin, ListView):
    model = AllergiesAdverseReactionAlert
    template_name = 'alerts/list.html'
    context_object_name = 'alerts'


class AlertUpdateView(CheckPatientMixin, AlertMessagesMixin, SuccessMessageMixin, RegisterActionMixing, UpdateView):
    model = AllergiesAdverseReactionAlert
    template_name = 'alerts/create-edit.html'
    form_class = AlertsCreateUpdateForm
    success_url = reverse_lazy('alerts:list-alert')
    success_message = "%(substance)s fue editado satisfactoriamente"

class AlertDeleteView(DeleteView):
    model = AllergiesAdverseReactionAlert
    success_url = reverse_lazy('alerts:list-alert')
