# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <table border="1" width="100%">
#   <thead>
#       <tr><th align="right">Date / Time: </th><th>Nov 14, 1999</th><th>April 7, 2000</th></tr>
#   </thead>
#   <tbody>
#       <tr><th align="left">Height</th><td>177 cm</td><td>177 cm</td></tr>
#       <tr><th align="left">Weight</th><td>86 kg</td><td>88 kg</td></tr>
#       <tr><th align="left">Blood Pressure</th><td>132/86 mmHg</td><td>145/88 mmHg</td></tr>
#   </tbody>
# </table>