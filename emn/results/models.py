# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <text>
#   <table border="1" width="100%">
#       <thead>
#           <tr><th>&#160;</th><th>March 23, 2000</th><th>April 06, 2000</th></tr>
#       </thead>
#       <tbody>
#           <tr><td colspan="3"><content styleCode="BoldItalics">Hematology</content></td></tr>
#           <tr><td>HGB (M 13-18 g/dl; F 12-16 g/dl)</td><td>13.2</td><td>&#160;</td></tr>
#           <tr><td>WBC (4.3-10.8 10+3/ul)</td><td>6.7</td><td>&#160;</td></tr>
#           <tr><td>PLT (135-145 meq/l)</td><td>123*</td><td>&#160;</td></tr>
#           <tr><td colspan="3"><content styleCode="BoldItalics">Chemistry</content></td></tr>
#           <tr><td>NA (135-145meq/l)</td><td>&#160;</td><td>140</td></tr>
#           <tr><td>K (3.5-5.0 meq/l)</td><td>&#160;</td><td>4.0</td></tr>
#           <tr><td>CL (98-106 meq/l)</td><td>&#160;</td><td>102</td></tr>
#           <tr><td>HCO3 (18-23 meq/l)</td><td>&#160;</td><td>35*</td></tr>
#       </tbody>
#   </table>
# </text>