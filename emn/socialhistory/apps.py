from django.apps import AppConfig


class SocialhistoryConfig(AppConfig):
    name = 'socialhistory'
