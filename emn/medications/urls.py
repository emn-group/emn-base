from django.conf.urls import url
from contrib.views import autocomplete_view
from .models import MedicationName
from .views import MedicationCreateView, MedicationListView, MedicationUpdateView, MedicationDeleteView

urlpatterns = [
    url(r'^insertar/$', MedicationCreateView.as_view(), name='create-medication'),
    url(r'^listar/$', MedicationListView.as_view(), name='list-medication'),
    url(r'^editar/(?P<pk>[0-9]+)/$', MedicationUpdateView.as_view(), name='edit-medication'),
    url(r'^borrar/(?P<pk>[0-9]+)/$', MedicationDeleteView.as_view(), name='delete-medication'),
    url(r'^medication/$', autocomplete_view, {'model': MedicationName}, name='medication'),
]
