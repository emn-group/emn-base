# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <table border="1" width="100%">
#   <thead>
#       <tr><th>Supply/Device</th><th>Date Supplied</th></tr>
#   </thead>
#   <tbody>
#       <tr><td>Automatic implantable cardioverter/defibrillator</td><td>Nov 1999</td></tr>
#       <tr><td>Total hip replacement prosthesis</td><td>1998</td></tr>
#       <tr><td>Wheelchair</td><td>1999</td></tr>
#   </tbody>
#  </table>