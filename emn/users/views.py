from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

# Models
from .models import PatientProxy
from patients.models import Patient
# Auth
from django.contrib.auth.decorators import login_required
from .forms import PatientCreationForm, PatientEditForm
from django.contrib.auth import views as auth_views
# Django Generic Views
from django.views.generic import CreateView, UpdateView
# Forms
from patients.forms import PatientDemographicEditForm
# mixin
from contrib.views import AlertMessagesMixin
# Create your views here.


class PatientCreate(CreateView):
    """
    CreateView with inline that creates an user with the
    associated patient
    """
    Model = PatientProxy
    form_class = PatientCreationForm
    form_inline = PatientDemographicEditForm
    template_name = 'users/create-patient.html'
    success_url = reverse_lazy('users:auth_redirect')

    def get_context_data(self, **kwargs):
        if 'form_inline' not in kwargs:
            kwargs['form_inline'] = self.form_inline
        return super(PatientCreate, self).get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        self.object = None
        form = self.get_form()
        form_inline = self.form_inline(request.POST)
        form_inline.user = form.instance
        if form.is_valid() and form_inline.is_valid():
            return self.form_valid(form, form_inline)
        else:
            return self.form_invalid(form, form_inline)

    def form_valid(self, form, form_inline):
        """
        If the form is valid, save the associated model with the
        inline and redirecs to the success url
        """
        self.object = form.save()
        patient = form_inline.save(commit=False)
        patient.user = form.instance
        patient.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form, form_inline):
        """
        Called if a form is invalid. Re-renders the context
        data the data-filled forms and errors.
        """
        return self.render_to_response(
            self.get_context_data(form=form, form_inline=form_inline))


class PatientUpdateView(AlertMessagesMixin, UpdateView):
    """docstring for PatientUpdateView"""
    Model = PatientProxy
    form_class = PatientEditForm
    template_name = 'users/patient-user-edit.html'
    success_url = reverse_lazy('users:auth_redirect')

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj
    
    def form_valid(self, form):
        tmp = super(PatientUpdateView, self).form_valid(form)
        patient = Patient.objects.get(user=self.request.user)
        patient.card_created = True
        patient.save()
        return tmp



def user_login_view(request):
    """
    Calling the django default login view
    """
    template_name = 'users/login.html'
    funct = auth_views.login(
        request,
        template_name,
        redirect_field_name='users:auth_redirect',
        redirect_authenticated_user=True)
    return funct


@login_required
def user_redirect(request):
    """
    View for redirecting users depending on their perms
    or role
    """
    user = request.user

    medic = user.groups.filter(name='medical-personnel').exists()
    if medic:
        return redirect('personnel:emergency-search')

    administrative = user.groups.filter(
        name='administrative-personnel').exists()

    if administrative:
        return redirect('personnel:emergency-search')

    patient = user.groups.filter(name='patient').exists()
    if patient:
        return redirect('patients:profile-patient')

    return redirect('admin:index')
