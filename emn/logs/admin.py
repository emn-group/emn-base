from django.contrib import admin

from .models import MedicalLog, LoginLog, EmergencyAccessLog
# Register your models here.


class MedicalLogModelAdmin(admin.ModelAdmin):
    """
    Settings for registering reactions to Substances
    """

    list_display = ('get_username', 'date',)
    # search_fields = ('get_username', 'date',)
    ordering = ('date',)
    readonly_fields = ('date', 'action_type', 'content_type',
                       'object_id', 'get_username', 'patient')
    # date_hierarchy = 'created_at'

    def get_username(self, obj):
        return obj.patient.user.username

    def has_add_permission(self, request):
        return False

admin.site.register(MedicalLog, MedicalLogModelAdmin)


class MedicalAccessLogModelAdmin(admin.ModelAdmin):
    """
    Settings for registering access to medical info
    """

    list_display = ('get_username', 'get_patient', 'date',)
    # search_fields = ('get_username', 'get_patient', 'date',)
    ordering = ('date',)
    readonly_fields = ('date', 'get_patient', 'get_username',)
    exclude = ('patient', 'user')

    def get_username(self, obj):
        return obj.user.username

    def get_patient(self, obj):
        if obj.patient:
            return obj.patient.user.username
        return 'None'

    def has_add_permission(self, request):
        return False

admin.site.register(EmergencyAccessLog, MedicalAccessLogModelAdmin)


class SessionModelAdmin(admin.ModelAdmin):
    """
    Settings for registering logins
    """

    list_display = ('get_username', 'date')
    # search_fields = ('user',)
    ordering = ('user', 'date')
    readonly_fields = ('date', 'get_username')
    # date_hierarchy = 'created_at'

    def get_username(self, obj):
        return obj.user.username
    
    def has_add_permission(self, request):
        return False

admin.site.register(LoginLog, SessionModelAdmin)
