from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import TemplateView

# Create your views here.

class LandingPageView(TemplateView):
    """
    TemplateView for the Landing Page
    """
    template_name = 'dashboard/landing.html'

