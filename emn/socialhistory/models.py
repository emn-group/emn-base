# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <table border="1" width="100%">
#       <thead>
#           <tr><th>Social History Element</th><th>Description</th><th>Effective Dates</th></tr>
#       </thead>
#       <tbody>
#           <tr><td>Cigarette smoking</td><td>1 pack per day</td><td>1947 - 1972</td></tr>
#           <tr><td>"</td><td>None</td><td>1973 - </td></tr>"
#           <tr><td>Alcohol consumption</td><td>None</td><td>1973 - </td></tr>          
#       </tbody>
#   </table>


class SocialHistoryElement(models.Model):

    class Meta:
        verbose_name = "Social History Element"
        verbose_name_plural = "Social History Elements"

    name = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name


class SocialHistory(models.Model):

    class Meta:
        verbose_name = "Social History"
        verbose_name_plural = "Social History"
    social_history_element = models.CharField(max_length=250)
    description = models.CharField(max_length=250)
    effective_dates = models.CharField(max_length=250)

    def __str__(self):
        return self.social_history_element
