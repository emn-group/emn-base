
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from django.core.urlresolvers import reverse_lazy

from django.db import transaction

from django.contrib.messages.views import SuccessMessageMixin

from patients.models import Patient

from contrib.views import (AddPatientCreateMixin,
                           ListPatientObjectsMixin,
                           AlertMessagesMixin,
                           CheckPatientMixin)

from .models import Problem

from .forms import ProblemCreateUpdateForm
# Create your views here.


class ProblemCreateView(CheckPatientMixin, SuccessMessageMixin, AddPatientCreateMixin, CreateView):
    model_class = Problem
    template_name = 'problems/create-edit.html'
    form_class = ProblemCreateUpdateForm
    success_url = reverse_lazy('problems:list-problem')
    success_message = "%(condition)s fue creado satisfactoriamente"


class ProblemListView(CheckPatientMixin, ListPatientObjectsMixin, AlertMessagesMixin, ListView):
    model = Problem
    template_name = 'problems/list.html'
    context_object_name = 'problems'


class ProblemUpdateView(CheckPatientMixin, SuccessMessageMixin, UpdateView):
    model = Problem
    template_name = 'problems/create-edit.html'
    form_class = ProblemCreateUpdateForm
    success_url = reverse_lazy('problems:list-problem')
    success_message = "%(condition)s fue editado satisfactoriamente"

class ProblemDeleteView(DeleteView):
    model = Problem
    form_class = ProblemCreateUpdateForm
    success_url = reverse_lazy('problems:list-problem')
