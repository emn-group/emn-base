
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from django.core.urlresolvers import reverse_lazy

from django.db import transaction

from django.contrib.messages.views import SuccessMessageMixin

from patients.models import Patient

from contrib.views import AddPatientCreateMixin, ListPatientObjectsMixin, AlertMessagesMixin, CheckPatientMixin

from logs.views import RegisterActionMixing

from .models import Procedure

from .forms import ProcedureCreateUpdateForm
# Create your views here.


class ProcedureCreateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, AddPatientCreateMixin, CreateView):
    model_class = Procedure
    template_name = 'procedures/create-edit.html'
    form_class = ProcedureCreateUpdateForm
    success_url = reverse_lazy('procedures:list-procedure')
    success_message = "%(procedure)s fue creado satisfactoriamente"


class ProcedureListView(CheckPatientMixin, ListPatientObjectsMixin, AlertMessagesMixin, ListView):
    model = Procedure
    template_name = 'procedures/list.html'
    context_object_name = 'procedures'


class ProcedureUpdateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, UpdateView):
    model = Procedure
    template_name = 'procedures/create-edit.html'
    form_class = ProcedureCreateUpdateForm
    success_url = reverse_lazy('procedures:list-procedure')
    success_message = "%(procedure)s fue editado satisfactoriamente"

class ProcedureDeleteView(DeleteView):
    model = Procedure
    form_class = ProcedureCreateUpdateForm
    success_url = reverse_lazy('procedures:list-procedure')
