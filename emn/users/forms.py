# -*- coding: utf-8 -*-
from django.utils.translation import ugettext_lazy as _

from django.forms import (ModelForm, CharField, PasswordInput, ValidationError)
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core import validators
from django.contrib.auth.password_validation import (UserAttributeSimilarityValidator,
                                                     CommonPasswordValidator,
                                                     NumericPasswordValidator)


from .models import PatientProxy, CustomUser, MedicalPersonnelProxy


class UserCreationForm(ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = CharField(
        label='Contraseña',
        widget=PasswordInput,
        help_text=_(
            "6 caracteres como mínimo, distingue mayúsculas de minúsculas"
            ),
        validators=[validators.MinLengthValidator(9,
                                                  'La contraseña debe tener \
                                                  por lo menos 9 caracteres'),
                    # UserAttributeSimilarityValidator,
                    # CommonPasswordValidator,
                    # NumericPasswordValidator
                   ]
        )


    password2 = CharField(label='Confirmación de contraseña', widget=PasswordInput)

    class Meta:
        model = CustomUser
        fields = ("username", "email")

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Las contraseñas no coinciden")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'password', 'date_of_birth', 'is_active', 'is_admin')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class PatientCreationForm(ModelForm):
    """
    A form that creates a user with 'patient' role, from the given username,
    email and password.
    """
    error_messages = {
        'password_mismatch': _("Las contraseñas no coinciden"),
    }

    password1 = CharField(label=_("Contraseña"),
                          widget=PasswordInput,
                          validators=[validators.MinLengthValidator(9,
                                                                    'La contraseña debe tener por lo menos 9 caracteres'),
                                    #   UserAttributeSimilarityValidator,
                                    #   CommonPasswordValidator,
                                    #   NumericPasswordValidator
                                     ]
                         )
    password2 = CharField(label=_("Confirmación de contraseña"),
                          widget=PasswordInput,
                          help_text=_(
                              "Introduzca la misma contraseña que arriba."
                              ))

    class Meta:
        model = PatientProxy
        fields = ("username", "email",)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(PatientCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class PatientEditForm(ModelForm):
    """
    A form that creates a user with 'patient' role, from the given username,
    email and password.
    """

    class Meta:
        model = PatientProxy
        fields = ("username", "email",)


class PatientDemographic(ModelForm):
    """
    A form for patient`s demographic inline form
    """
    class Meta:
        model = PatientProxy
        fields = ("username", "email",)


class UserProxyCreateUpdateForm(ModelForm):
    """A form for creating new Medical Personnel."""
    password1 = CharField(label=_("Password"),
                          widget=PasswordInput, required=False, min_length=6)
    class Meta:
        model = MedicalPersonnelProxy
        fields = ("username", "email", "first_name",
                  "last_name", "identity_document", "date_of_birth", "password1"
                  , "is_active")

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserProxyCreateUpdateForm, self).save(commit=False)
        print(self.cleaned_data["password1"])
        if len(self.cleaned_data["password1"]) > 1:
            user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user
