# -*- coding: utf-8 -*-
from django.db import models

from patients.models import Patient

# Create your models here.

#REFERENCE
# <tr><th>Directive</th><th>Description</th><th>Verification</th><th>Supporting Document(s)</th></tr>
# <tr>
#   <td>Resuscitation status</td> 
#   <td><content ID="AD1">Do not resuscitate</content></td> 
#   <td>Dr. Robert Dolin, Nov 07, 1999</td>
#   <td><linkHtml href="AdvanceDirective.b50b7910-7ffb-4f4c-bbe4-177ed68cbbf3.pdf">Advance directive</linkHtml></td>
# </tr>


class SupportingDocument(models.Model):

    class Meta:
        verbose_name = "Supporting Document"
        verbose_name_plural = "Supporting Documents"

    document = models.FileField(upload_to='uploads/%Y/%m/%d/')

    def __str__(self):
        pass

    def get_name():
        return "Advance directive"


class AdvancedDirective(models.Model):

    class Meta:
        verbose_name = "Advanced Directive"
        verbose_name_plural = "Advanced Directives"

    patient = models.ForeignKey(Patient, null=True)
    directive = models.CharField(max_length=250)
    code = models.CharField(max_length=250)
    description = models.CharField(max_length=250)
    verification = models.CharField(max_length=250)
    supporting_document = models.ForeignKey(SupportingDocument)

    def __str__(self):
        return self.directive
