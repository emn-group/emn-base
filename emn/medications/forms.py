# -*- coding: utf-8 -*-
from .models import Medication

from django.forms import ModelForm, RadioSelect, TypedChoiceField


class MedicationCreateUpdateForm(ModelForm):
    """
    Form that creates and edits patient`s Medications
    """
    YESNO_CHOICES = (('0', 'Compartido'), ('1', 'Privado'))
    private = TypedChoiceField(choices=YESNO_CHOICES, widget=RadioSelect, label='Estado',
                               help_text='La información marcada como privada no puede ser accedida por el personal médico')

    class Meta:

        model = Medication
        fields = ('medication',
                  'instructions',
                  'start_date',
                  'status',
                  'private',
                  )
