from django.conf.urls import url
from contrib.views import autocomplete_view
from .models import ProcedureNameCode

from .views import ProcedureCreateView, ProcedureListView, ProcedureUpdateView, ProcedureDeleteView

urlpatterns = [
    url(r'^insertar/$', ProcedureCreateView.as_view(), name='create-procedure'),
    url(r'^listar/$', ProcedureListView.as_view(), name='list-procedure'),
    url(r'^editar/(?P<pk>[0-9]+)/$', ProcedureUpdateView.as_view(), name='edit-procedure'),
    url(r'^borrar/(?P<pk>[0-9]+)/$', ProcedureDeleteView.as_view(), name='delete-procedure'),
    url(r'^procedure/$', autocomplete_view, {'model': ProcedureNameCode}, name='procedure'),
]
