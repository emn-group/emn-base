from django.conf.urls import url

from contrib.views import autocomplete_view

from .models import Condition
from .views import ProblemCreateView, ProblemListView, ProblemUpdateView, ProblemDeleteView

urlpatterns = [
    url(r'^insertar/$', ProblemCreateView.as_view(), name='create-problem'),
    url(r'^listar/$', ProblemListView.as_view(), name='list-problem'),
    url(r'^editar/(?P<pk>[0-9]+)/$', ProblemUpdateView.as_view(), name='edit-problem'),
    url(r'^borrar/(?P<pk>[0-9]+)/$', ProblemDeleteView.as_view(), name='delete-problem'),
    url(r'^condition/$', autocomplete_view, {'model': Condition}, name='condition'),
]
