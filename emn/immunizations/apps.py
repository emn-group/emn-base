from django.apps import AppConfig


class InmunizationsConfig(AppConfig):
    name = 'immunizations'
