# -*- coding: utf-8 -*-
from django.db import models

from patients.models import Patient

# Create your models here.

# <text>
# <table border="1" width="100%">
#   <thead>
#       <tr><th>Procedure</th><th>Date</th></tr>
#   </thead>
#   <tbody>
#       <tr><td><content ID="Proc1">Total hip replacement, left</content></td><td>1998</td></tr>
#   </tbody>
# </table>  


class ProcedureNameCode(models.Model):

    class Meta:
        verbose_name = "Procedimiento"
        verbose_name_plural = "Procedimientos"
        unique_together = (("name", "code"),)

    name = models.CharField(max_length=250, verbose_name="Nombre")
    code = models.CharField(max_length=250, verbose_name="Código")

    def __str__(self):
        return self.name


class Procedure(models.Model):

    class Meta:
        verbose_name = "Procedure"
        verbose_name_plural = "Procedures"

    patient = models.ForeignKey(Patient, null=True)
    procedure = models.CharField(max_length=250, verbose_name = "Intervención")
    procedure_code = models.CharField(max_length=250, verbose_name = "Código", blank=True, null=True)
    date = models.DateField(max_length=250, verbose_name = "Fecha", null=True, blank=True)
    private =  models.BooleanField(default=True, verbose_name = "Estado")

    def __str__(self):
        return self.procedure
