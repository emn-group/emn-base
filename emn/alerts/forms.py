# -*- coding: utf-8 -*-
from .models import AllergiesAdverseReactionAlert

from django.forms.widgets import HiddenInput

from django.forms import ModelForm, RadioSelect, TypedChoiceField


class AlertsCreateUpdateForm(ModelForm):
    """
    Form that creates and edits patient`s Allergies, Adverse Reactions, Alerts
    """

    YESNO_CHOICES = (('0', 'Compartido'), ('1', 'Privado'))
    private = TypedChoiceField(choices=YESNO_CHOICES, widget=RadioSelect, label='Estado',
                               help_text='La información marcada como privada no puede ser accedida por el personal médico')

    class Meta:

        model = AllergiesAdverseReactionAlert
        fields = ('substance',
                  'substance_code',
                  'reaction',
                  'status',
                  'private',)

        widgets = {'substance_code': HiddenInput}
