
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from django.core.urlresolvers import reverse_lazy

from django.db import transaction

from django.contrib.messages.views import SuccessMessageMixin

from patients.models import Patient

from contrib.views import AddPatientCreateMixin, ListPatientObjectsMixin, AlertMessagesMixin, CheckPatientMixin

from logs.views import RegisterActionMixing

from .models import Medication

from .forms import MedicationCreateUpdateForm
# Create your views here.


class MedicationCreateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, AddPatientCreateMixin, CreateView):
    model_class = Medication
    template_name = 'medications/create-edit.html'
    form_class = MedicationCreateUpdateForm
    success_url = reverse_lazy('medications:list-medication')
    success_message = "%(medication)s fue creado satisfactoriamente"


class MedicationListView(CheckPatientMixin, ListPatientObjectsMixin, AlertMessagesMixin, ListView):
    model = Medication
    template_name = 'medications/list.html'
    context_object_name = 'medications'


class MedicationUpdateView(CheckPatientMixin, SuccessMessageMixin, AlertMessagesMixin, RegisterActionMixing, UpdateView):
    model = Medication
    template_name = 'medications/create-edit.html'
    form_class = MedicationCreateUpdateForm
    success_url = reverse_lazy('medications:list-medication')
    success_message = "%(medication)s fue editado satisfactoriamente"


class MedicationDeleteView(DeleteView):
    model = Medication
    form_class = MedicationCreateUpdateForm
    success_url = reverse_lazy('medications:list-medication')
