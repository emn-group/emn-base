# -*- coding: utf-8 -*-
from django.shortcuts import HttpResponse
from django.contrib.auth.views import redirect_to_login, logout_then_login
from django.contrib import messages
from patients.models import Patient
from django.db import transaction
import json



# Create your views here.
def autocomplete_view(request, model):
    """
    A view for handling jquery autocomplete
    """
    if request.is_ajax():
        filter_term = request.GET.get('term', '')
        search_dict = {'name__icontains': filter_term}
        model_list = model.objects.filter(**search_dict)[:20]
        results = []
        for response in model_list:
            response_json = {}
            response_json['id'] = response.id
            response_json['label'] = response.name
            try:
                response_json['code'] = response.code
            except:
                pass
            results.append(response_json)
        data = json.dumps(results)
    else:
        data = 'fail'
    mimetype = 'application/json'
    return HttpResponse(data, mimetype)


class CheckUserGroupsMixin(object):
    """
    A Mixin for checking if an user is in a determined
    collection of groups
    """

    def dispatch(self, request, *args, **kwargs):
        """ A function that checks if an user is in a determined
        collection of groups """
        if not request.user.groups.filter(name__in=[
                "medical-personnel", "administrative-personnel"]):
            return logout_then_login(request)
        if request.user.is_active is False:
            logout_then_login(request)

        return super(CheckUserGroupsMixin, self).dispatch(
            request, *args, **kwargs)


class CheckLoginMixin(object):
    """
    A Mixin for checking if an user is loged in
    """

    def dispatch(self, request, *args, **kwargs):
        """ A function that checks if an user is in a determined
        collection of groups """
        if not request.user.is_authenticated():
            return redirect_to_login(request)

        return super(CheckLoginMixin, self).dispatch(
            request, *args, **kwargs)

class CheckPatientMixin(object):
    """
    A Mixin for checking if an user is in group 'patient' and
    has an Patient user proxy
    """

    def dispatch(self, request, *args, **kwargs):
        """ A function that checks if an user is in a determined
        collection of groups """

        if not request.user.groups.filter(name__in=[
                "patient"]):
            return logout_then_login(request)

        if not Patient.objects.filter(user=request.user):
            return logout_then_login(request)

        return super(CheckPatientMixin, self).dispatch(
            request, *args, **kwargs)


class AddPatientCreateMixin(object):

    @transaction.atomic
    def form_valid(self, form):
        resp = super(AddPatientCreateMixin, self).form_valid(form)
        self.object.patient = Patient.objects.get(user=self.request.user)
        self.object.save()
        return resp


class ListPatientObjectsMixin(object):

    def get_queryset(self):
        user = self.request.user
        patient = Patient.objects.get(user=user)
        obj = self.model.objects.filter(patient=patient)
        return obj


class AlertMessagesMixin(object):
    """
    Adds alert messages
    """

    def render_to_response(self, context, **response_kwargs):
        response = super(AlertMessagesMixin, self).render_to_response(context, **response_kwargs)
        alert_messages = self.get_alert_messages()
        for alert in alert_messages:
            messages.error(self.request, alert)
        return response

    def get_alert_messages(self):
        alert_messages = []
        patient = Patient.objects.get(user=self.request.user)
        if patient.card_created is True:
            alert_messages.append(
                'Su tarjeta de emergencia esta desactualizada, por favor creé una nueva'
                )
        return alert_messages
