# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-03-02 16:24
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='administrativepersonnelproxy',
            options={'verbose_name': 'Personal administrativo', 'verbose_name_plural': 'Personal administrativo'},
        ),
        migrations.AlterModelOptions(
            name='adminmanagerproxy',
            options={'verbose_name': 'Administrador de manejadores', 'verbose_name_plural': 'Administradores de manejadores'},
        ),
        migrations.AlterModelOptions(
            name='institutionmanager',
            options={'verbose_name': 'Manejador de instituciones', 'verbose_name_plural': 'Manejadores de instituciones'},
        ),
        migrations.AlterModelOptions(
            name='institutionproxy',
            options={'verbose_name': 'Institución', 'verbose_name_plural': 'Instituciones'},
        ),
        migrations.AlterModelOptions(
            name='medicalpersonnelproxy',
            options={'verbose_name': 'Personal médico', 'verbose_name_plural': 'Personal médico'},
        ),
        migrations.AlterModelOptions(
            name='patientmanagerproxy',
            options={'verbose_name': 'Manejador de pacientes', 'verbose_name_plural': 'Manejadores de pacientes'},
        ),
        migrations.AlterModelOptions(
            name='patientproxy',
            options={'verbose_name': 'Paciente', 'verbose_name_plural': 'Pacientes'},
        ),
        migrations.AlterModelOptions(
            name='personnelmanagerproxy',
            options={'verbose_name': 'Manejador de personal', 'verbose_name_plural': 'Manejadores de personal'},
        ),
    ]
