# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.
# <table border="1" width="100%">
#   <thead>
#       <tr><th>Encounter</th><th>Location</th><th>Date</th></tr>
#   </thead>
#   <tbody>
#       <tr><td>Checkup Examination</td><td>Good Health Clinic</td><td>Apr 07, 2000</td></tr>
#   </tbody>
# </table>


class EncounterName(models.Model):

    class Meta:
        verbose_name = "Encounter"
        verbose_name_plural = "Encounters"

    name = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name


class Location(models.Model):

    class Meta:
        verbose_name = "Location"
        verbose_name_plural = "Locations"

    name = models.CharField(max_length=250, unique=True)

    def __str__(self):
        return self.name


class Encounter(models.Model):

    class Meta:
        verbose_name = "Encounter"
        verbose_name_plural = "Encounters"
    encounter = models.CharField(max_length=250)
    location = models.CharField(max_length=250)
    date = models.DateField(max_length=250)

    def __str__(self):
        return self.encounter
