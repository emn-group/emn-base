# -*- coding: utf-8 -*-
from django.forms import ModelForm, RadioSelect, TypedChoiceField
from django.forms.widgets import HiddenInput
from .models import Relative, FamilyHistory


class RelativeCreateUpdateForm(ModelForm):
    """
    Form that creates and edits patient`s Relatives
    """
    class Meta:
        model = Relative
        fields = ('first_name',
                  'last_name',
                  'relation',
                  'phone_number',
                  'status',)


class FamilyHistoryCreateUpdateForm(ModelForm):
    """
    Form that creates and edits patient`s Family History
    """
    YESNO_CHOICES = (('0', 'Compartido'), ('1', 'Privado'))
    private = TypedChoiceField(choices=YESNO_CHOICES, widget=RadioSelect, label='Estado',
                               help_text='La información marcada como privada no puede ser accedida por el personal médico')

    def __init__(self, *args, **kwargs):
        patient = kwargs.pop('patient', None)
        super(FamilyHistoryCreateUpdateForm, self).__init__(*args, **kwargs) # populates the post
        self.fields['relative'].queryset = Relative.objects.filter(patient=patient)

    class Meta:
        model = FamilyHistory
        fields = ('diagnosis',
                  'diagnosis_code',
                  'relative',
                  'age_at_onset',
                  'private',)
        widgets = {'diagnosis_code': HiddenInput}

