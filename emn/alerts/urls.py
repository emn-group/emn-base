from django.conf.urls import url

from contrib.views import autocomplete_view

from .models import Substance, Reaction
from .views import AlertCreateView, AlertListView, AlertDeleteView, AlertUpdateView

urlpatterns = [
    url(r'^insertar/$', AlertCreateView.as_view(), name='create-alert'),
    url(r'^listar/$', AlertListView.as_view(), name='list-alert'),
    url(r'^editar/(?P<pk>[0-9]+)/$', AlertUpdateView.as_view(), name='edit-alert'),
    url(r'^borrar/(?P<pk>[0-9]+)/$', AlertDeleteView.as_view(), name='delete-alert'),
    url(r'^substance/$', autocomplete_view, {'model': Substance}, name='substance'),
    url(r'^reaction/$', autocomplete_view, {'model': Reaction}, name='reaction'),
]
