# -*- coding: utf-8 -*-
from django.db import models

# Create your models here.

# <title>Plan</title>
#   <text>
#   <table border="1" width="100%">
#       <thead>
#       <tr><th>Planned Activity</th><th>Planned Date</th></tr>
#       </thead>
#       <tbody>
#       <tr><td>Pulmonary function test</td><td>April 21, 2000</td></tr>
#       </tbody>
#   </table>
# </text>   