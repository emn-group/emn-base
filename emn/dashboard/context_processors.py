#from django.conf import settings # import the settings file

def pages(request):
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {
        'TITLE': 'SIE',
        'protocol': 'http',
        'domain': 'vicott.pythonanywhere.com/',
        #input formats
        'phone':'99999999999999',
        'dateRange':'9999-9999',
        'date':'99/99/9999',
        'message_consent': """"""
        }
