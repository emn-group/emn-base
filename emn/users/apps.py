from __future__ import unicode_literals

from django.db.models import signals
from django.apps import AppConfig


class UsersConfig(AppConfig):
    """
    An AppConfig for creating roles and permissions on migration
    """
    name = 'users'
    def ready(self):
       
        from .signals import roles_perms, password_change_signal, create_user_profile_signal
        from .models import (CustomUser, AdminUserProfile, MedicalPersonnelProxy,
                             AdministrativePersonnelProxy, PersonnelManagerProxy, AdminManagerProxy)

        signals.post_migrate.connect(roles_perms, sender=self)

        signals.post_save.connect(password_change_signal,
                                 sender=CustomUser, dispatch_uid='accounts.password_change_signal')

        signals.post_save.connect(create_user_profile_signal,
                                  sender=CustomUser,
                                  dispatch_uid='accounts.create_user_profile_signal')


        signals.post_save.connect(password_change_signal,
                                 sender=MedicalPersonnelProxy,
                                 dispatch_uid='MedicalPersonnelProxy.password_change_signal')

        signals.post_save.connect(create_user_profile_signal,
                                  sender=MedicalPersonnelProxy,
                                  dispatch_uid='MedicalPersonnelProxy.create_user_profile_signal')


        signals.post_save.connect(password_change_signal,
                                 sender=AdministrativePersonnelProxy,
                                 dispatch_uid='AdministrativePersonnelProxy.password_change_signal')

        signals.post_save.connect(create_user_profile_signal,
                                  sender=AdministrativePersonnelProxy,
                                  dispatch_uid='AdministrativePersonnelProxy.create_user_profile_signal')


        signals.post_save.connect(password_change_signal,
                                 sender=PersonnelManagerProxy,
                                 dispatch_uid='PersonnelManagerProxy.password_change_signal')

        signals.post_save.connect(create_user_profile_signal,
                                  sender=PersonnelManagerProxy,
                                  dispatch_uid='PersonnelManagerProxy.create_user_profile_signal')


        signals.post_save.connect(password_change_signal,
                                 sender=AdminManagerProxy,
                                 dispatch_uid='AdminManagerProxy.password_change_signal')

        signals.post_save.connect(create_user_profile_signal,
                                  sender=AdminManagerProxy,
                                  dispatch_uid='AdminManagerProxy.create_user_profile_signal')
