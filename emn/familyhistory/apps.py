from django.apps import AppConfig


class FamilyhistoryConfig(AppConfig):
    name = 'familyhistory'
